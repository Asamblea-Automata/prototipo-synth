#Side Wave Attack.

Este sintetizador nace como una exploracion cientifica y artistica de los instrumentos electronicos.
A travez de la modelacion por medio de programacion grafica y el diseño de una interfaz ya pensado,
el esqueleto general de un sintetizador es:

#Oscilador: 
Modulo encargado de generar la señal de audio. Esto lo hace a travez de formas de onda que se generan de forma analogica o digital. En este caso es un DCO (Digital Controlled Oscillator).

Estructura:
	Selector de forma de Onda.
	Rango / Escala: Permite determinar la tesitura en la onda que genera el oscilador.
	Afinacion: Division en 12 semitonos (¿Se podrá hacer mas largo?).


#Mixer: 
Permite nivelar la amplitud de cada oscilador.

Estructura:
	Faders para cada oscilador

#Filtro (VCF): 
Sirve para limpiar la señal. En este caso el filtro sirve para quedarte con las grecuencias que te interesan, ya sean graves, medias o agudas. Los mas conocidos son low pass, hi pass, band pass

Estructura:
	Cutoff: Es el punto en donde el filtro va a empezar a cortar las frecuencias que no necesita.
	Resonancia: La resonacia es una incremento o decremento del punto de corte.
	Knobs para low pass, hi pass, band pass.
#Amplificador (VCA): 
Controla la amplitud de la señal.

#Envolvente (ENV): 
Permite modular el Attack, Decay, Sustein y Release.

Estructura:
	Knobs para cada tipo de envolvente

#Low Frequency Oscillator (LFO): 
Es un oscilador de baja frecuencia. Sirven para modular la señal de audio. Cuenta con los mismos parametros que los de un oscilador convencional(¿Se podran agregar filtros?)

Estructura:
	Selector de forma de onda.
	Rate: Velocidad a la que oscila el LFO

El desarrollo se llevará de la forma en la que está descrito el sintetizador.
