import processing.sound.*; //Libreria que permite usar osciladores, filtros, ENV, etc.
import themidibus.*; //Libreria que permite de forma simplificada conectar dispositivos midi con el codigo

int midiNote = 0; //Corresponde al valor midi cuando se pulsa una tecla.

//MIDI

MidiBus myBus; //Definir un objeto de tipo MidiBus
int indc=0; //Integer que almacenara el indice al que corresponde el controlador midi
String[] inputs=MidiBus.availableInputs(); //Lista con todos los controladores midi que estan conectados.


// OSCILADORES


SinOsc sinOsc; //Oscilador sinusoidal
SawOsc sawOsc; //Oscilador diente de sierra
SqrOsc sqrOsc; //Oscilador de onda cuadrada
TriOsc triOsc; //Oscilador de onda triagular
Pulse pulse; //Oscilador de pulso

float snAmp=0; //Amplitud del Oscilador sinusoidal
float swAmp=0; //Amplitud del Oscilador diente de sierra
float srAmp=0; //Amplitud del Oscilador de onda cuadrada
float trAmp=0; //Amplitud del Oscilador de onda triagular
float puAmp=0; //Amplitud del Oscilador de pulso

float snFR=0; //Frecuencia del Oscilador sinusoidal
float swFR=0; //Frecuencia del Oscilador diente de sierra
float srFR=0; //Frecuencia del Oscilador de onda cuadrada
float trFR=0; //Frecuencia del Oscilador de onda triagular
float puFR=0; //Frecuencia del Oscilador de pulso
//Filter
/*
  Se definen todos los filtros que extiende la libreria.
  Ninguno esta conectado, puesto que la documentacion sobre
  ellos es limitada. 
*/
LowPass lowPass;
HighPass highPass;
BandPass bandPass;
Delay delay;
Reverb reverb;
/*
  Se definen las variables para los filtros
*/
float cutOff=0;
float resonance=0;
float dltime=0;
float dlfBack=0;
float rvDamp=0;
float roomSize=0;

//ENV

Env env; //Se define el filtro de Envolvente.
float attackT = 0;
float sustainT = 0;
float sustainL = 0;
float releaseT = 0;

//Conectar controlador

void conectar(String name){
  MidiBus.list(); //Se enlistan todos los dispositivos conectados
  for(int i=0; i<inputs.length; i++){ //Se realiza una busqueda lineal hasta encontrar el input buscado
    if(inputs[i].contains(name)){
      indc=i;
      break;
    }
  }
  myBus= new MidiBus(this, indc, -1); //Se crea el objeto midi bus, con una entrada definida y con la salida predeterminada 
}

//Calculo de frecuencia midi https://en.wikipedia.org/wiki/MIDI_tuning_standard

float midiToFreq(int note){
  return (pow(2, ((note-69)/12.0)))*440;
}

void __init__(){ //Se inicializan todos los objetos que fueron definidos anteriormente
  
  sinOsc = new SinOsc (this);
  sawOsc = new SawOsc (this);
  sqrOsc = new SqrOsc (this); 
  triOsc = new TriOsc (this);
  pulse = new Pulse (this);
  lowPass = new LowPass (this);
  highPass = new HighPass (this);
  bandPass = new BandPass (this);
  delay = new Delay (this);
  reverb = new Reverb (this);
  env = new Env (this);
  
}

void noteOn(int channel, int pitch, int velocity){ //Esta funcion es llamada cada vez que se pulsa una nota
  
  //Se apagan todos los osciladores y los filtros
  sinOsc.stop();
  sawOsc.stop();
  sqrOsc.stop();
  triOsc.stop();
  pulse.stop();
  lowPass.stop();
  highPass.stop();
  bandPass.stop();
  delay.stop();
  reverb.stop();
  
  //Se obtienen los valores de cada Slider de la seccion de oscilador
  snAmp = SineSld.getValueF(); 
  swAmp = SawSld.getValueF();
  srAmp = sqrSld.getValueF();
  trAmp = TriSld.getValueF();
  puAmp = PulseSld.getValueF();
  //Se obtienen los valores de cada knob de la seccion de modulador de frecuencia.
  snFR = knobFSine.getValueF();
  swFR = knobFSaw.getValueF();
  srFR = knobFSQR.getValueF();
  trFR = knobFTri.getValueF();
  puFR = knobFPulse.getValueF();
  //Se obtienen los valores del slider 2D, donde el cutOff corresponde al eje X y resonance al eje y
  cutOff = sliderCRes.getValueXF();
  resonance = sliderCRes.getValueYF();
  
  //Se obtienen los valores de los knobs de Delay y reverb
  dltime = tTime.getValueF();
  dlfBack = feedBack.getValueF();
  rvDamp = dAmp.getValueF();
  roomSize = sizeRoom.getValueF();
  
  //Se obtienen los valores de cada Slider de la seccion de envolventes
  attackT = sliderAttack.getValueF();
  sustainT = sliderDecay.getValueF();
  sustainL =  sliderSustain.getValueF();
  releaseT =  sliderRelease.getValueF();
  
  midiNote = pitch; //El pitch corresponde al valor midi de la nota pulsada.
  
  if(SineSld.getValueF() > 0){ //Si el valor del Slider es mayor que cero entonces
    sinOsc.play(midiToFreq(midiNote) + snFR, snAmp); //Se activa el oscilador, con la nota midi transformada a la frecuencia y la amplitud obtenida
    env.play(sinOsc, attackT, sustainT, sustainL, releaseT); //Se activa el envolvente, que recibe un oscilador el los parametros attack, dekay, sustain y release
  }
  
  if(SawSld.getValueF()>0){ //Si el valor del Slider es mayor que cero entonces
    sawOsc.play(midiToFreq(midiNote) + swFR, swAmp); //Se activa el oscilador, con la nota midi transformada a la frecuencia y la amplitud obtenida
    env.play(sawOsc, attackT, sustainT, sustainL, releaseT); //Se activa el envolvente, que recibe un oscilador el los parametros attack, dekay, sustain y release
    
  }
  
  if(sqrSld.getValueF()>0){ //Si el valor del Slider es mayor que cero entonces
    sqrOsc.play(midiToFreq(midiNote) + srFR, srAmp); //Se activa el oscilador, con la nota midi transformada a la frecuencia y la amplitud obtenida
    env.play(sqrOsc, attackT, sustainT, sustainL, releaseT); //Se activa el envolvente, que recibe un oscilador el los parametros attack, dekay, sustain y release
  }
  
  if(TriSld.getValueF()>0){ //Si el valor del Slider es mayor que cero entonces
    triOsc.play(midiToFreq(midiNote) + trFR, trAmp); //Se activa el oscilador, con la nota midi transformada a la frecuencia y la amplitud obtenida
    env.play(triOsc, attackT, sustainT, sustainL, releaseT); //Se activa el envolvente, que recibe un oscilador el los parametros attack, dekay, sustain y release
  }
 
  if(PulseSld.getValueF()>0){ //Si el valor del Slider es mayor que cero entonces
    pulse.play(midiToFreq(midiNote) + puFR, puAmp); //Se activa el oscilador, con la nota midi transformada a la frecuencia y la amplitud obtenida
    env.play(pulse, attackT, sustainT, sustainL, releaseT); //Se activa el envolvente, que recibe un oscilador el los parametros attack, dekay, sustain y release
  }
  
}

void noteOff(int channel, int pitch, int velocity) {
   
}
