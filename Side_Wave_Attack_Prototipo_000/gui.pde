/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

public void panel1_Click1(GPanel source, GEvent event) { //_CODE_:panel1:477190:
  println("panel1 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel1:477190:

public void custom_slider3_change1(GCustomSlider source, GEvent event) { //_CODE_:sqrSld:905971:
  println("custom_slider3 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:sqrSld:905971:

public void custom_slider4_change1(GCustomSlider source, GEvent event) { //_CODE_:TriSld:593844:
  println("custom_slider4 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:TriSld:593844:

public void custom_slider5_change1(GCustomSlider source, GEvent event) { //_CODE_:PulseSld:714451:
  println("custom_slider5 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:PulseSld:714451:

public void custom_slider1_change1(GCustomSlider source, GEvent event) { //_CODE_:SineSld:707403:
  println("SineSld - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:SineSld:707403:

public void custom_slider1_change2(GCustomSlider source, GEvent event) { //_CODE_:SawSld:450661:
  println("SawSld - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:SawSld:450661:

public void panel2_Click1(GPanel source, GEvent event) { //_CODE_:panel2:362420:
  println("panel2 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel2:362420:

public void custom_slider1_change3(GCustomSlider source, GEvent event) { //_CODE_:sliderAttack:383601:
  println("custom_slider1 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:sliderAttack:383601:

public void custom_slider2_change1(GCustomSlider source, GEvent event) { //_CODE_:sliderDecay:475863:
  println("custom_slider2 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:sliderDecay:475863:

public void custom_slider3_change2(GCustomSlider source, GEvent event) { //_CODE_:sliderSustain:443608:
  println("custom_slider3 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:sliderSustain:443608:

public void custom_slider4_change2(GCustomSlider source, GEvent event) { //_CODE_:sliderRelease:556886:
  println("custom_slider4 - GCustomSlider >> GEvent." + event + " @ " + millis());
} //_CODE_:sliderRelease:556886:

public void panel3_Click1(GPanel source, GEvent event) { //_CODE_:panel3:678473:
  println("panel3 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel3:678473:

public void slider2d1_change1(GSlider2D source, GEvent event) { //_CODE_:sliderCRes:851093:
  println("slider2d1 - GSlider2D >> GEvent." + event + " @ " + millis());
} //_CODE_:sliderCRes:851093:

public void button1_click1(GButton source, GEvent event) { //_CODE_:buttonLow:845032:
  println("button1 - GButton >> GEvent." + event + " @ " + millis());
} //_CODE_:buttonLow:845032:

public void button2_click1(GButton source, GEvent event) { //_CODE_:buttonHigh:579904:
  println("button2 - GButton >> GEvent." + event + " @ " + millis());
} //_CODE_:buttonHigh:579904:

public void button3_click1(GButton source, GEvent event) { //_CODE_:buttonBand:877888:
  println("button3 - GButton >> GEvent." + event + " @ " + millis());
} //_CODE_:buttonBand:877888:

public void panel4_Click1(GPanel source, GEvent event) { //_CODE_:panel4:948256:
  println("panel4 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel4:948256:

public void knob1_turn1(GKnob source, GEvent event) { //_CODE_:tTime:279654:
  println("knob1 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:tTime:279654:

public void knob2_turn1(GKnob source, GEvent event) { //_CODE_:feedBack:430636:
  println("knob2 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:feedBack:430636:

public void panel5_Click1(GPanel source, GEvent event) { //_CODE_:Reverb:820733:
  println("Reverb - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:Reverb:820733:

public void knob3_turn1(GKnob source, GEvent event) { //_CODE_:dAmp:228037:
  println("knob3 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:dAmp:228037:

public void knob4_turn1(GKnob source, GEvent event) { //_CODE_:sizeRoom:830512:
  println("knob4 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:sizeRoom:830512:

public void panel5_Click2(GPanel source, GEvent event) { //_CODE_:panel5:253706:
  println("panel5 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel5:253706:

public void knob1_turn2(GKnob source, GEvent event) { //_CODE_:knobFSine:312276:
  println("knob1 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:knobFSine:312276:

public void knob2_turn2(GKnob source, GEvent event) { //_CODE_:knobFSaw:227256:
  println("knob2 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:knobFSaw:227256:

public void knob3_turn2(GKnob source, GEvent event) { //_CODE_:knobFSQR:302296:
  println("knob3 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:knobFSQR:302296:

public void knob4_turn2(GKnob source, GEvent event) { //_CODE_:knobFTri:846811:
  println("knob4 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:knobFTri:846811:

public void knob5_turn1(GKnob source, GEvent event) { //_CODE_:knobFPulse:920558:
  println("knob5 - GKnob >> GEvent." + event + " @ " + millis());
} //_CODE_:knobFPulse:920558:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setMouseOverEnabled(false);
  surface.setTitle("Sketch Window");
  panel1 = new GPanel(this, 37, 21, 327, 237, "Oscilador");
  panel1.setText("Oscilador");
  panel1.setOpaque(true);
  panel1.addEventHandler(this, "panel1_Click1");
  sqrSld = new GCustomSlider(this, 186, 22, 169, 60, "grey_blue");
  sqrSld.setRotation(PI/2, GControlMode.CORNER);
  sqrSld.setLimits(0.0, 0.0, 1.0);
  sqrSld.setNumberFormat(G4P.DECIMAL, 2);
  sqrSld.setOpaque(false);
  sqrSld.addEventHandler(this, "custom_slider3_change1");
  TriSld = new GCustomSlider(this, 249, 21, 169, 60, "grey_blue");
  TriSld.setRotation(PI/2, GControlMode.CORNER);
  TriSld.setLimits(0.0, 0.0, 1.0);
  TriSld.setNumberFormat(G4P.DECIMAL, 2);
  TriSld.setOpaque(false);
  TriSld.addEventHandler(this, "custom_slider4_change1");
  PulseSld = new GCustomSlider(this, 317, 22, 169, 60, "grey_blue");
  PulseSld.setRotation(PI/2, GControlMode.CORNER);
  PulseSld.setLimits(0.0, 0.0, 1.0);
  PulseSld.setNumberFormat(G4P.DECIMAL, 2);
  PulseSld.setOpaque(false);
  PulseSld.addEventHandler(this, "custom_slider5_change1");
  label1 = new GLabel(this, 2, 210, 63, 20);
  label1.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label1.setText("Sine");
  label1.setOpaque(false);
  label2 = new GLabel(this, 66, 210, 62, 20);
  label2.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label2.setText("Saw");
  label2.setOpaque(false);
  label3 = new GLabel(this, 128, 208, 60, 20);
  label3.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label3.setText("SQR");
  label3.setOpaque(false);
  label4 = new GLabel(this, 189, 207, 61, 24);
  label4.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label4.setText("Triangle");
  label4.setOpaque(false);
  label5 = new GLabel(this, 255, 206, 62, 20);
  label5.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label5.setText("Pulse");
  label5.setOpaque(false);
  SineSld = new GCustomSlider(this, 61, 20, 169, 60, "grey_blue");
  SineSld.setRotation(PI/2, GControlMode.CORNER);
  SineSld.setLimits(0, 0, 1);
  SineSld.setNumberFormat(G4P.INTEGER, 0);
  SineSld.setOpaque(false);
  SineSld.addEventHandler(this, "custom_slider1_change1");
  SawSld = new GCustomSlider(this, 123, 21, 169, 60, "grey_blue");
  SawSld.setRotation(PI/2, GControlMode.CORNER);
  SawSld.setLimits(0.0, 0.0, 1.0);
  SawSld.setNumberFormat(G4P.DECIMAL, 2);
  SawSld.setOpaque(false);
  SawSld.addEventHandler(this, "custom_slider1_change2");
  panel1.addControl(sqrSld);
  panel1.addControl(TriSld);
  panel1.addControl(PulseSld);
  panel1.addControl(label1);
  panel1.addControl(label2);
  panel1.addControl(label3);
  panel1.addControl(label4);
  panel1.addControl(label5);
  panel1.addControl(SineSld);
  panel1.addControl(SawSld);
  panel2 = new GPanel(this, 373, 143, 233, 192, "Env");
  panel2.setText("Env");
  panel2.setOpaque(true);
  panel2.addEventHandler(this, "panel2_Click1");
  sliderAttack = new GCustomSlider(this, 66, 24, 129, 64, "grey_blue");
  sliderAttack.setRotation(PI/2, GControlMode.CORNER);
  sliderAttack.setLimits(0.0, 0.0, 1.0);
  sliderAttack.setNumberFormat(G4P.DECIMAL, 2);
  sliderAttack.setOpaque(false);
  sliderAttack.addEventHandler(this, "custom_slider1_change3");
  sliderDecay = new GCustomSlider(this, 122, 25, 129, 64, "grey_blue");
  sliderDecay.setRotation(PI/2, GControlMode.CORNER);
  sliderDecay.setLimits(0.0, 0.0, 1.0);
  sliderDecay.setNumberFormat(G4P.DECIMAL, 2);
  sliderDecay.setOpaque(false);
  sliderDecay.addEventHandler(this, "custom_slider2_change1");
  sliderSustain = new GCustomSlider(this, 180, 25, 129, 64, "grey_blue");
  sliderSustain.setRotation(PI/2, GControlMode.CORNER);
  sliderSustain.setLimits(0.0, 0.0, 1.0);
  sliderSustain.setNumberFormat(G4P.DECIMAL, 2);
  sliderSustain.setOpaque(false);
  sliderSustain.addEventHandler(this, "custom_slider3_change2");
  sliderRelease = new GCustomSlider(this, 231, 25, 129, 64, "grey_blue");
  sliderRelease.setRotation(PI/2, GControlMode.CORNER);
  sliderRelease.setLimits(0.0, 0.0, 1.0);
  sliderRelease.setNumberFormat(G4P.DECIMAL, 2);
  sliderRelease.setOpaque(false);
  sliderRelease.addEventHandler(this, "custom_slider4_change2");
  label6 = new GLabel(this, -1, 166, 65, 20);
  label6.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label6.setText("Attack");
  label6.setOpaque(false);
  label7 = new GLabel(this, 169, 166, 65, 20);
  label7.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label7.setText("Release");
  label7.setOpaque(false);
  label8 = new GLabel(this, 106, 167, 65, 20);
  label8.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label8.setText("Sustain");
  label8.setOpaque(false);
  label9 = new GLabel(this, 48, 166, 66, 20);
  label9.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label9.setText("Decay");
  label9.setOpaque(false);
  panel2.addControl(sliderAttack);
  panel2.addControl(sliderDecay);
  panel2.addControl(sliderSustain);
  panel2.addControl(sliderRelease);
  panel2.addControl(label6);
  panel2.addControl(label7);
  panel2.addControl(label8);
  panel2.addControl(label9);
  panel3 = new GPanel(this, 39, 267, 322, 205, "Filter");
  panel3.setText("Filter");
  panel3.setOpaque(true);
  panel3.addEventHandler(this, "panel3_Click1");
  sliderCRes = new GSlider2D(this, 6, 26, 312, 103);
  sliderCRes.setLimitsX(0.5, 0.0, 20000.0);
  sliderCRes.setLimitsY(0.5, 0.0, 20000.0);
  sliderCRes.setNumberFormat(G4P.DECIMAL, 2);
  sliderCRes.setOpaque(true);
  sliderCRes.addEventHandler(this, "slider2d1_change1");
  buttonLow = new GButton(this, 3, 156, 80, 30);
  buttonLow.setText("Low Pass");
  buttonLow.addEventHandler(this, "button1_click1");
  buttonHigh = new GButton(this, 110, 155, 80, 30);
  buttonHigh.setText("High Pass");
  buttonHigh.addEventHandler(this, "button2_click1");
  buttonBand = new GButton(this, 231, 154, 80, 30);
  buttonBand.setText("Band Pass");
  buttonBand.addEventHandler(this, "button3_click1");
  panel3.addControl(sliderCRes);
  panel3.addControl(buttonLow);
  panel3.addControl(buttonHigh);
  panel3.addControl(buttonBand);
  panel4 = new GPanel(this, 371, 339, 120, 109, "Delay");
  panel4.setText("Delay");
  panel4.setOpaque(true);
  panel4.addEventHandler(this, "panel4_Click1");
  tTime = new GKnob(this, 1, 22, 44, 45, 0.8);
  tTime.setTurnRange(110, 70);
  tTime.setTurnMode(GKnob.CTRL_HORIZONTAL);
  tTime.setSensitivity(1);
  tTime.setShowArcOnly(false);
  tTime.setOverArcOnly(false);
  tTime.setIncludeOverBezel(false);
  tTime.setShowTrack(true);
  tTime.setLimits(0.0, 0.0, 180.0);
  tTime.setShowTicks(true);
  tTime.setOpaque(false);
  tTime.addEventHandler(this, "knob1_turn1");
  feedBack = new GKnob(this, 70, 22, 44, 45, 0.8);
  feedBack.setTurnRange(110, 70);
  feedBack.setTurnMode(GKnob.CTRL_HORIZONTAL);
  feedBack.setSensitivity(1);
  feedBack.setShowArcOnly(false);
  feedBack.setOverArcOnly(false);
  feedBack.setIncludeOverBezel(false);
  feedBack.setShowTrack(true);
  feedBack.setLimits(0.0, 0.0, 180.0);
  feedBack.setShowTicks(true);
  feedBack.setOpaque(false);
  feedBack.addEventHandler(this, "knob2_turn1");
  label10 = new GLabel(this, 6, 70, 39, 20);
  label10.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label10.setText("TIME");
  label10.setOpaque(false);
  label11 = new GLabel(this, 64, 71, 53, 20);
  label11.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label11.setText("BACK");
  label11.setOpaque(false);
  panel4.addControl(tTime);
  panel4.addControl(feedBack);
  panel4.addControl(label10);
  panel4.addControl(label11);
  Reverb = new GPanel(this, 500, 339, 118, 104, "Reverb");
  Reverb.setText("Reverb");
  Reverb.setOpaque(true);
  Reverb.addEventHandler(this, "panel5_Click1");
  dAmp = new GKnob(this, 1, 22, 44, 45, 0.8);
  dAmp.setTurnRange(110, 70);
  dAmp.setTurnMode(GKnob.CTRL_HORIZONTAL);
  dAmp.setSensitivity(1);
  dAmp.setShowArcOnly(false);
  dAmp.setOverArcOnly(false);
  dAmp.setIncludeOverBezel(false);
  dAmp.setShowTrack(true);
  dAmp.setLimits(0.0, 0.0, 180.0);
  dAmp.setShowTicks(true);
  dAmp.setOpaque(false);
  dAmp.addEventHandler(this, "knob3_turn1");
  sizeRoom = new GKnob(this, 70, 23, 44, 45, 0.8);
  sizeRoom.setTurnRange(110, 70);
  sizeRoom.setTurnMode(GKnob.CTRL_HORIZONTAL);
  sizeRoom.setSensitivity(1);
  sizeRoom.setShowArcOnly(false);
  sizeRoom.setOverArcOnly(false);
  sizeRoom.setIncludeOverBezel(false);
  sizeRoom.setShowTrack(true);
  sizeRoom.setLimits(0.0, 0.0, 180.0);
  sizeRoom.setShowTicks(true);
  sizeRoom.setOpaque(false);
  sizeRoom.addEventHandler(this, "knob4_turn1");
  label12 = new GLabel(this, 3, 71, 50, 20);
  label12.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label12.setText("DAMP");
  label12.setOpaque(false);
  label13 = new GLabel(this, 61, 72, 55, 20);
  label13.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label13.setText("SIZE");
  label13.setOpaque(false);
  Reverb.addControl(dAmp);
  Reverb.addControl(sizeRoom);
  Reverb.addControl(label12);
  Reverb.addControl(label13);
  panel5 = new GPanel(this, 368, 21, 250, 116, "MODULADOR DE FRECUENCIA");
  panel5.setText("MODULADOR DE FRECUENCIA");
  panel5.setOpaque(true);
  panel5.addEventHandler(this, "panel5_Click2");
  knobFSine = new GKnob(this, 2, 20, 44, 45, 0.8);
  knobFSine.setTurnRange(110, 70);
  knobFSine.setTurnMode(GKnob.CTRL_HORIZONTAL);
  knobFSine.setSensitivity(1);
  knobFSine.setShowArcOnly(false);
  knobFSine.setOverArcOnly(false);
  knobFSine.setIncludeOverBezel(false);
  knobFSine.setShowTrack(true);
  knobFSine.setLimits(0.0, 0.0, 512.0);
  knobFSine.setShowTicks(true);
  knobFSine.setOpaque(false);
  knobFSine.addEventHandler(this, "knob1_turn2");
  knobFSaw = new GKnob(this, 52, 20, 44, 45, 0.8);
  knobFSaw.setTurnRange(110, 70);
  knobFSaw.setTurnMode(GKnob.CTRL_HORIZONTAL);
  knobFSaw.setSensitivity(1);
  knobFSaw.setShowArcOnly(false);
  knobFSaw.setOverArcOnly(false);
  knobFSaw.setIncludeOverBezel(false);
  knobFSaw.setShowTrack(true);
  knobFSaw.setLimits(0.0, 0.0, 512.0);
  knobFSaw.setShowTicks(true);
  knobFSaw.setOpaque(false);
  knobFSaw.addEventHandler(this, "knob2_turn2");
  knobFSQR = new GKnob(this, 99, 21, 44, 45, 0.8);
  knobFSQR.setTurnRange(110, 70);
  knobFSQR.setTurnMode(GKnob.CTRL_HORIZONTAL);
  knobFSQR.setSensitivity(1);
  knobFSQR.setShowArcOnly(false);
  knobFSQR.setOverArcOnly(false);
  knobFSQR.setIncludeOverBezel(false);
  knobFSQR.setShowTrack(true);
  knobFSQR.setLimits(0.0, 0.0, 512.0);
  knobFSQR.setShowTicks(true);
  knobFSQR.setOpaque(false);
  knobFSQR.addEventHandler(this, "knob3_turn2");
  knobFTri = new GKnob(this, 148, 20, 44, 45, 0.8);
  knobFTri.setTurnRange(110, 70);
  knobFTri.setTurnMode(GKnob.CTRL_HORIZONTAL);
  knobFTri.setSensitivity(1);
  knobFTri.setShowArcOnly(false);
  knobFTri.setOverArcOnly(false);
  knobFTri.setIncludeOverBezel(false);
  knobFTri.setShowTrack(true);
  knobFTri.setLimits(0.0, 0.0, 512.0);
  knobFTri.setShowTicks(true);
  knobFTri.setOpaque(false);
  knobFTri.addEventHandler(this, "knob4_turn2");
  knobFPulse = new GKnob(this, 200, 20, 44, 45, 0.8);
  knobFPulse.setTurnRange(110, 70);
  knobFPulse.setTurnMode(GKnob.CTRL_HORIZONTAL);
  knobFPulse.setSensitivity(1);
  knobFPulse.setShowArcOnly(false);
  knobFPulse.setOverArcOnly(false);
  knobFPulse.setIncludeOverBezel(false);
  knobFPulse.setShowTrack(true);
  knobFPulse.setLimits(0.0, 0.0, 512.0);
  knobFPulse.setShowTicks(true);
  knobFPulse.setOpaque(false);
  knobFPulse.addEventHandler(this, "knob5_turn1");
  label14 = new GLabel(this, 3, 74, 46, 20);
  label14.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label14.setText("sine");
  label14.setOpaque(false);
  label15 = new GLabel(this, 58, 74, 39, 20);
  label15.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label15.setText("Saw");
  label15.setOpaque(false);
  label16 = new GLabel(this, 104, 74, 45, 20);
  label16.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label16.setText("SQR");
  label16.setOpaque(false);
  label17 = new GLabel(this, 151, 73, 45, 20);
  label17.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label17.setText("^");
  label17.setOpaque(false);
  label18 = new GLabel(this, 201, 73, 46, 20);
  label18.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label18.setText("Pulse");
  label18.setOpaque(false);
  panel5.addControl(knobFSine);
  panel5.addControl(knobFSaw);
  panel5.addControl(knobFSQR);
  panel5.addControl(knobFTri);
  panel5.addControl(knobFPulse);
  panel5.addControl(label14);
  panel5.addControl(label15);
  panel5.addControl(label16);
  panel5.addControl(label17);
  panel5.addControl(label18);
}

// Variable declarations 
// autogenerated do not edit
GPanel panel1; 
GCustomSlider sqrSld; 
GCustomSlider TriSld; 
GCustomSlider PulseSld; 
GLabel label1; 
GLabel label2; 
GLabel label3; 
GLabel label4; 
GLabel label5; 
GCustomSlider SineSld; 
GCustomSlider SawSld; 
GPanel panel2; 
GCustomSlider sliderAttack; 
GCustomSlider sliderDecay; 
GCustomSlider sliderSustain; 
GCustomSlider sliderRelease; 
GLabel label6; 
GLabel label7; 
GLabel label8; 
GLabel label9; 
GPanel panel3; 
GSlider2D sliderCRes; 
GButton buttonLow; 
GButton buttonHigh; 
GButton buttonBand; 
GPanel panel4; 
GKnob tTime; 
GKnob feedBack; 
GLabel label10; 
GLabel label11; 
GPanel Reverb; 
GKnob dAmp; 
GKnob sizeRoom; 
GLabel label12; 
GLabel label13; 
GPanel panel5; 
GKnob knobFSine; 
GKnob knobFSaw; 
GKnob knobFSQR; 
GKnob knobFTri; 
GKnob knobFPulse; 
GLabel label14; 
GLabel label15; 
GLabel label16; 
GLabel label17; 
GLabel label18; 
