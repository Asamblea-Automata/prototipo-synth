/*

  CREADOR: Asamblea de Dios Automata
  
*/



// Need G4P library
import g4p_controls.*;
// You can remove the PeasyCam import if you are not using
// the GViewPeasyCam control or the PeasyCam library.
import peasy.*;


public void setup(){
  size(660, 600, JAVA2D);
  createGUI(); //Se crea la interfaz
  customGUI();
  // Place your setup code here
  conectar("play"); //Nombre del controlador
  __init__(); //Se inicializa el sintetizador
}

public void draw(){
  background(230);
  
}

// Use this method to add additional statements
// to customise the GUI controls
public void customGUI(){

}
